/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2melina;

import java.util.ArrayList;

/**
 *
 * @author Melina Gomez
 */
public class ObjetoPersonas {
    private Integer cedula;
    private String nombre;
    private String genero;
    
    public static ArrayList<ObjetoPersonas> listaPersonas = new ArrayList<ObjetoPersonas>();
   

    public ObjetoPersonas(Integer cedula, String nombre, String genero) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.genero = genero;
    }
    public ObjetoPersonas(int cedula,String nombre) {
        this.cedula = cedula;
        this.nombre = nombre;
    }

    public Integer getCedula() {
        return cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
    
}
