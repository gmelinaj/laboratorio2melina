/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2melina;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Melina Gomez
 */
public class Laboratorio2Melina {
 //static HashMap Diccionario = new HashMap();
 static ArrayList<String> listareporte = new ArrayList<>();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int opcion;
        Scanner sc = new Scanner(System.in);
        boolean sigue = true;
       
        while (sigue==true){
            System.out.println("");
            System.out.println("-------Menú-------");
            System.out.println("1.Registro de personas");
            System.out.println("2.Registro de mascotas");
            System.out.println("3.Adoptar mascota");
            System.out.println("4.Reporte");
            System.out.println("5.Salir");
            System.out.print("Opcion: ");
            opcion = sc.nextInt();
            System.out.println("");
            switch (opcion) {
                case 1:
                    Registropersonas rp= new Registropersonas();
                    rp.registrarper();
                    
                    sigue=true;
                    break;
                    
                case 2:
                    Registromascotas rm= new Registromascotas();
                    rm.registrarmasc();
                    sigue=true;
                   
                    break;
                case 3:
                    AdoptarMascotas am= new AdoptarMascotas();
                    listareporte=am.adoptarmascotas(listareporte);
                    sigue=true;
                   
                    break;
                case 4:
                    Reporte rr= new Reporte();
                    rr.reporte(listareporte);
                   
                    break;
                
                case 5:
                    sigue=false;
                    break;
                default:
                    System.out.printf("Opcion no valida");
                    break;
            }

        } 
       
    }
    
}
