/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio2melina;

import java.util.ArrayList;

/**
 *
 * @author Melina Gomez
 */
public class ObjetoMascotas {
    private static int id;
    private Integer idMascota;
    private String nombre;
    private String tipo;
    private String estado;
    
    public static ArrayList<ObjetoMascotas> listaMascotas = new ArrayList<>();

    public ObjetoMascotas( String nombre, String tipo, String estado) {
        this.idMascota = id++;
        this.nombre = nombre;
        this.tipo = tipo;
        this.estado = estado;
    }

    public Integer getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(Integer idMascota) {
        this.idMascota = idMascota;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
   

}

